<?php
namespace App\CanvasToVideo;

class CanvasToVideo {

	public function convertVideo($data) {
		$framePath = base_path('canvas-to-video/frames');
		$phantomJs = base_path('canvas-to-video/phantom/draw.js');

		if (!\File::exists($framePath))
		{
			exec('mkdir '.$framePath);
		}
		if (!\File::exists($phantomJs))
		{
			echo 'please put draw.js in ' . $phantomjs;
			die();
		}
		
		$this->createFrame($data);		
		$pieces = explode('/', $data['bannerUrl']);
		$dir = $pieces[count($pieces)-1];

		$videoPath = $this->encodeVideoFromFrame($dir, $data['duration']);
		return $videoPath;
	}

	private function encodeVideoFromFrame($frameDirectoryName = '', $duration) {
		$frame_path = base_path("canvas-to-video/frames/".$frameDirectoryName);
		exec('cd '.$frame_path .' && ffmpeg -framerate 15/1 -start_number 1 -t '.$duration.' -i canvas-%d.png -vcodec mpeg4 video.avi');
		return base_path("canvas-to-video/frames/".$frameDirectoryName."/video.avi");
	}

	private function createFrame($args) {
		$pieces = explode('/', $args['bannerUrl']);
		$dir = $pieces[count($pieces)-1];
		$args['frame_dir'] = base_path('canvas-to-video/frames/'.$dir);
		$cmd = "phantomjs " . base_path("canvas-to-video/phantom/draw.js " . implode(' ', $args));
		$resp = exec($cmd);
	}
}